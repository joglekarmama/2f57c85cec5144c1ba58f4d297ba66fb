import Phaser from 'phaser';
import config from 'visual-config-exposer';

class Instructions extends Phaser.Scene {
  constructor() {
    super('instructions');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  create() {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    const bg = this.add.image(0, 0, 'background').setOrigin(0, 0).setScale(5);

    this.add.text(
      this.GAME_WIDTH / 5,
      this.GAME_HEIGHT / 2.5 + 50,
      config.game.instruction1,
      {
        fontSize: '24px',
        fill: '#000',
      }
    );
    this.add.text(
      this.GAME_WIDTH / 5,
      this.GAME_HEIGHT / 2.5 + 100,
      config.game.instruction2,
      {
        fontSize: '24px',
        fill: '#000',
      }
    );
    this.add.text(
      this.GAME_WIDTH / 5,
      this.GAME_HEIGHT / 2.5 + 150,
      config.game.instruction3,
      {
        fontSize: '24px',
        fill: '#000',
      }
    );
    this.add.text(
      this.GAME_WIDTH / 5,
      this.GAME_HEIGHT / 2.5 + 200,
      config.game.instruction4,
      {
        fontSize: '24px',
        fill: '#000',
      }
    );

    let ball = this.physics.add.image(350, 200, 'ball');
    ball.setScale(0.3);
    ball.body.allowGravity = false;
    ball.body.immovable = true;
    ball.body.moves = false;

    this.tweens.add({
      targets: ball,
      rotation: 6.28319,
      duration: 2000,
      ease: 'Linear',
      repeat: -1,
      //   yoyo: true,
    });

    let pointer = this.physics.add.image(400, 300, 'pointer');
    pointer.setRotation(-0.7);
    pointer.setScale(0.1);
    pointer.body.allowGravity = false;
    pointer.body.immovable = true;
    pointer.body.moves = false;

    this.tweens.add({
      targets: pointer,
      y: 200,
      duration: 2000,
      ease: 'Sine.easeInOut',
      repeat: -1,
      yoyo: true,
    });

    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 2 + 250,
      'Click To Play',
      {
        fontSize: '32px',
        fill: '#000',
        fontStyle: 'bold',
      }
    );

    let hoverImage = this.add.image(100, 100, 'startBall');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    playBtn.setInteractive();

    playBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = playBtn.x - playBtn.width / 2;
      hoverImage.y = playBtn.y + 10;
    });
    playBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    playBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('playGame');
    });

    this.scale.on('resize', this.resize, this);
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;
    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;
    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default Instructions;
